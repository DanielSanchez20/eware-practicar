# Python Setup environment

Windows
    
```powershell
python3 -m venv .\environment

# In CMD
.\environment\Scripts\activate.bat
# In PowerShell
.\environment\Scripts\Activate.ps1

pip install -r .\environment\requirements.txt
```

MacOS or Linux

```bash
$ python3 -m venv environment/
$ . environment/bin/activate
$ pip install -r environment/requirements.txt