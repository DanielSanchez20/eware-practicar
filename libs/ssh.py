from environment.config import SSHClient, AutoAddPolicy, time

class RemoteInfo:
    passwd = ""
    username = ""
    port = None
    hostname = ""

    def __init__(self, username, passwd, hostname, port=None):
        self.passwd = str(passwd)
        self.username = str(username)
        self.port = port
        self.hostname = str(hostname)

    def getDiskInfo(self):
        client = SSHClient()
        client.set_missing_host_key_policy(AutoAddPolicy())
        client.load_host_keys("C:/Users/Daniel/.ssh/known_hosts")
        client.load_system_host_keys()

        if (self.port != None):
            client.connect(self.hostname,port=self.port, username=self.username, password=self.passwd)
        else:
            client.connect(self.hostname, username=self.username, password=self.passwd)
        client.exec_command("df -H -v / > disk_info.txt")

        time.sleep(2)

        sftp = client.open_sftp()
        sftp.get(remotepath="disk_info.txt",localpath="files/disk_info.txt")

        sftp.close()
        client.close()

    def getMemInfo(self):
        client = SSHClient()
        client.set_missing_host_key_policy(AutoAddPolicy())
        client.load_host_keys("C:/Users/Daniel/.ssh/known_hosts")
        client.load_system_host_keys()

        if (self.port != None):
            client.connect(self.hostname,port=self.port, username=self.username, password=self.passwd)
        else:
            client.connect(self.hostname, username=self.username, password=self.passwd)
        client.exec_command("top -b -n 1 -o %MEM | head -n 4 | tail -n 1 > mem_info.txt")

        time.sleep(2)

        sftp = client.open_sftp()
        sftp.get(remotepath="mem_info.txt",localpath="files/mem_info.txt")
        
        sftp.close()
        client.close()

    def getTopThreeApps(self):
        client = SSHClient()
        client.set_missing_host_key_policy(AutoAddPolicy())
        client.load_host_keys("C:/Users/Daniel/.ssh/known_hosts")
        client.load_system_host_keys()

        if (self.port != None):
            client.connect(self.hostname,port=self.port, username=self.username, password=self.passwd)
        else:
            client.connect(self.hostname, username=self.username, password=self.passwd)
        client.exec_command("top -b -n 1 -o %MEM | head -n 10 | tail -n 4 > mem_apps_info.txt")

        time.sleep(2)

        sftp = client.open_sftp()
        sftp.get(remotepath="mem_apps_info.txt",localpath="files/mem_apps_info.txt")
        
        sftp.close()
        client.close()