from environment.config import *
from libs.ssh import RemoteInfo

class Menu:
    remoteServer = RemoteInfo(username="root", passwd="3250", hostname="localhost", port=2200)

    def __init__(self):
        pass

    def printMenu(self):
        self.clearConsole()
        print("=================================")
        print("| Consultor de recursos remotos |")
        print("=================================")
        print("| Obtener:                      |")
        print("|    1. Espacio disponible      |")
        print("|    2. RAM disponible          |")
        print("|    3. Top 3 procesos          |")
        print("|                               |")
        print("|    0. Salir                   |")
        print("=================================")
        print()
        print("Opcion a elegir: ", end="")

    def showOutput(self, option):
        if (option == 1):
            print("Getting info...")
            self.remoteServer.getDiskInfo()
        elif (option == 2):
            print("Getting info...")
            self.remoteServer.getMemInfo()
        elif (option == 3):
            print("Getting info...")
            self.remoteServer.getTopThreeApps()
        elif (option != 0):
            print("Option not found")

    def clearConsole(self):
        # Windows
        if os.name in ("nt", "dos"):
            os.system("cls")
        # Linux
        else:
            os.system("clear")