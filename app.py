from tools.menu import Menu

def main():
    menu = Menu()
    user_option = None

    while (user_option != 0):
        menu.printMenu()
        
        user_option = int(input())
        menu.showOutput(user_option)

if __name__ == "__main__":
    main()